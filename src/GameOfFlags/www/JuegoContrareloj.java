package GameOfFlags.www;

import java.util.Random;
import java.util.Vector;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

public class JuegoContrareloj extends Activity implements OnClickListener,OnTouchListener,AnimationListener {
    /** Called when the activity is first created. */
	
	/*
	 * Hacemos una instancia del objeto Variables y de objeto Tools, el cual nos proporcionar� todas las variables 
	 * usadas en el programa y todas la funciones auxiliares que podamos necesitar.
	 */
	
    Variables var = new Variables();
    Tools tools =new Tools(); 
    Datos datos=new Datos();


	Tiempo tiempo;
	 
	Animation anima,anim1,anim2,anim3;
	
	ScrollView scroll;
	
	int segundos=0;
	int minutos=0;
	long milis_timer=0;

	
	boolean tiempo_acabado=false;
	boolean animacion_on=true;
	boolean boolean_pausa=false;
	boolean vibrar=false;
	boolean boolean_sonido=true;
	boolean juego_finalizado=false;
	

	
	ImageView imagen_animacion;
	
	Animation score,anime;
	
	LinearLayout lin_juego;
	LinearLayout lin,lin_general;
	
	Button bpausa,bmenu,bvolver,bsonido;
	
	TextView text;
	
	Vector<Object> vdatos_pausa,vindices_usados;
	
	MediaPlayer mp;

	
	SoundManager snd;
	int sonido_acierto=0,sonido_fallo=0;
	
	
	String nivel="facil";
	
	
	
    /******************************************************************************************************************/
    

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contrareloj);
        
       // requestWindowFeature(Window.FEATURE_NO_TITLE);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
      
        Typeface type_letra = Typeface.createFromAsset(getAssets(), Variables.FUENTE_LETRA_PROGRAMA);
        
        var.vector_iconos_cj = new Vector<Object>();
        var.vector_peliculas_cj = new Vector<Object>();
        
     
        
/*
 * asociamos los ImageView, los Buttons y activamos la interfaz de los clicks de nuestro juego        
 */
        
        
        //ImageView smiles
        for(int i=0;i<var.id_imagenes_cj.length;i++){
        	var.imagenes_cj[i]=(ImageView)findViewById(var.id_imagenes_cj[i]);
        }
        
        //ImageView comodines
        
        for(int i=0;i<var.id_comodines_cj.length;i++){
        	var.comodines_cj[i]=(ImageView)findViewById(var.id_comodines_cj[i]);
        }      
        
        imagen_animacion=(ImageView)findViewById(R.id.imagen_animacion);
        
        
        for(int i=0;i<var.botones_cj.length;i++){
        	var.botones_cj[i]=(Button)findViewById(var.id_botones_cj[i]);
        	var.botones_cj[i].setTextColor(Color.WHITE);
        	var.botones_cj[i].setTypeface(type_letra);
        	var.botones_cj[i].setOnClickListener(this);
        	var.botones_cj[i].setOnTouchListener(this);
        }
        
        bpausa=(Button)findViewById(R.id.bpausa);
        bpausa.setOnClickListener(this);
        
        bsonido=(Button)findViewById(R.id.bsonido);
        bsonido.setOnClickListener(this);

        
 
        
/***********************************************************************************************************************/
    	
    	/*
    	 * Probamos lo siguiente: que al pulsar un boton se genere un numero aleatorio, el cual se introducira en el array que tenemos 
    	 * almacenados los indices de secuencia de nuestra pelicula, una vez que tenemos esos numero se almacenan en un vector, con ese vector
    	 * y con ayuda de un bucle for conseguimos representar en pantalla la pelicula que queramos.
    	 * 
    	 */
        


    	//texto=(TextView)findViewById(R.id.Texto);


		//texto.setText("");
        
        
        var.tscore_cj=(TextView)findViewById(R.id.tscore);
        var.tscore_cj.setTextColor(Color.WHITE);
        var.tscore_cj.setTypeface(type_letra);
       
        
        
        var.ttime_cj=(TextView)findViewById(R.id.ttime);
        var.ttime_cj.setTextColor(Color.WHITE);
        var.ttime_cj.setTypeface(type_letra);
        
	
        for(int i=0;i<var.imagenes_score.length;i++){
        	var.imagenes_score[i]=(ImageView)findViewById(var.id_imagenes_score[i]);
        }
        
		
		
		
		lin_juego=(LinearLayout)findViewById(R.id.lin_smiley);
		
		
		
		
		


		
		var.lin_imagen_cj = (LinearLayout)findViewById(R.id.lin_smiley);


		var.lincomodin_cj = (LinearLayout)findViewById(R.id.linearlayoutcomodin);
		
		lin_general=(LinearLayout)findViewById(R.id.lin_cj_gen);
		
		segundos=Variables.SEGUNDOS_TIEMPO_CONTRARELOJ_CJ;
		minutos=Variables.MINUTOS_CONTRARELOJ_CJ;
		
		
		/*
		 * llamamos a funciones que en un primer lugar, nos borre toda la pantalla de los componentes, es decir que nos deje la pantalla vacia, 
		 * posteriormente llamamos a la funcion Generar Animacion que se encarga de generar una animacion con una cuenta regresiva de 3 segundos
		 */
		
		vdatos_pausa = new Vector<Object>();
		vindices_usados = new Vector<Object>();
		
		
		snd= new SoundManager(this);
		
		
		
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC); 
    	sonido_acierto= snd.load(R.raw.sonido_acierto);
    	sonido_fallo= snd.load(R.raw.sonido_fallo);
    	
		mp= MediaPlayer.create(this, R.raw.main_theme);
		mp.setLooping(true);
		mp.start();
    	//startService(new Intent(this, MusicaDeFondo.class));
    	
		
	      
      	cargarConfiguracion();
		borrar_componentes();
		
		Generar_Animacion();


    }
  /*****************************************************************************************************************************************************/
  
    @Override
    public void onDestroy(){
    	
    	  //Evento "Cerrar aplicaci�n" guardardamos los datos que queramos en un fichero xml 
      super.onDestroy();
      
      mp.release();
      
     // stopService(new Intent(this, MusicaDeFondo.class));
      
     
    } 
    
    @Override
    public void onResume(){
      super.onResume();
      
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
      
      if(boolean_sonido==true){
    	  mp.start();
      }
      
      if(boolean_pausa==true){
    	  pausar_juego();

      }
     
    } 
    
    

    @Override
    protected void onStart()
    {
    	//evento "Abrir aplicaci�n" leemos configuraci�n del fichero xml
    	
      super.onStart();
      cargarConfiguracion();
    }    

	
	protected void onPause() {
		
		super.onPause();
		if(animacion_on==false){
			tiempo.cancel();
			
		}
		
		if(boolean_sonido==true){
			mp.pause();
		}
		
		guardarConfiguracion();
		
		pausar_juego();	
		lin.setVisibility(View.INVISIBLE);
			
	}
	

    
	@Override
	public void onBackPressed() {
		//boton back
		
	//	iniciar_Activity_finish();
		
		if(animacion_on==false){
			if(boolean_pausa==true){
				Reanudar_juego();
			}else{
				pausar_juego();
			}
		}

		return;
	}
	
		

/****************************************************************************************************************************************************/	
	
	@Override
	public void onClick(View v) {
		
		
		
		/*
		 * funcion que pone todos los fondos de los botones en botones default
		 */
		
		botones_default();

		
/******************************************************************************************************************************************/
		
		//Se nos abre un dialogo para testear el juego mediante un texto
		
//		if(v.getId()==R.id.button2){
//			
//			
//			tiempo.cancel();
//			segundos=segundos+31;
//			
//			if(segundos>59){
//				segundos=segundos-60;
//				minutos++;
//			}
//			
//			//a�adimos 30 segundos al tiempo
//			iniciar_temporizador(milis_timer+Variables.SEGUNDOS_A�ADIR_CON_COMODIN, Variables.ESCALADO_TIEMPO_CONTRARELOJ_CJ);
//			var.num_score_cj=Variables.SCORE_ANADIR_COMODIN + var.num_score_cj;
//			var.tscore_cj.setText(getText(R.string.score)+" "+var.num_score_cj);
//			iniciar_dialog_test();
//			
//
//			
//		}
		
		if(v.getId()==R.id.bpausa){
			
			pausar_juego();

		}
		
		if(v.getId()==R.id.bsonido){
			boolean_sonido=!boolean_sonido;
			
			gestionar_sonido(boolean_sonido);
			

			

		}
		
/****************************************************************************************************************************************************/
		
		/*
		 * Se testea que boton ha producido la interrupci�n, llama a una funcion para comprobar si el texto que hay en el corresponde con el 
		 * codigo de indice de los iconos, si es cierto pone el boton de color verde, en caso contrario pone el boton rojo.
		 */
		
		for(int i=0;i<var.id_botones_cj.length;i++){
			
			if(v.getId()==var.id_botones_cj[i]){
				
				//chequear_respuesta(i);
				
			}

		}	
		
/****************************************************************************************************************************************************/

	
		if(v.equals(bmenu)){
			
			
				mp.stop();
				Intent j = new Intent(this, Menu_Principal.class);
				startActivity(j);
				finish();
			

		}
		
		if(v.equals(bvolver)){
			Reanudar_juego();
		}
	}
	
	
	

	@Override
	public boolean onTouch(View v, MotionEvent event) {
	
		
		for(int i=0;i<var.id_botones_cj.length;i++){
			
			if(v.getId()==var.id_botones_cj[i]){
				
				if(event.getAction()==MotionEvent.ACTION_DOWN){
					
					chequear_respuesta(i);
					
				}
				if(event.getAction()==MotionEvent.ACTION_UP){
					
					botones_default();
					Comodines(var.num_comodines_cj);
					generarjuego();
					
				}
				
	
		}
		
		
	}
		
			
	
		return false;
			
		
	}
	
/****************************************************************************************************************************************************/

	private void iniciar_dialog_test() {
	
	
	Dialog d= new Dialog(this);
	d.setTitle("Registro De Tiradas");
	new TextView(this);
	
	String stest="";
	//TextView texto = new TextView(this);
	//texto.setText("hello");
	
	stest="indice_c�digo: "+var.indice_codigo_cj;
	stest=stest+"\nindice_pelicula"+ var.indice_pelicula_cj;
	stest=stest+"\n aciertos ="+ var.acierto_cj;
	stest=stest+"\n fallos ="+ var.fallos_cj ;
	stest=stest+"\n N�Comodines ="+ var.num_comodines_cj; 
	//stest=stest+"\n Respuesta Correcta  ="+ getString(datos.titulos_peliculas_id[var.indice_codigo_cj]); 
	
	var.texto_cj = new TextView(this);
	var.texto_cj.setText(stest);
	
	scroll=new ScrollView(this);
	scroll.addView(var.texto_cj);
	d.setContentView(scroll);
	
	d.show();
	
}
	
/****************************************************************************************************************************************************/


	private void iniciar_Activity_finish() {
	/*
	 * creamos una activity al finalizar	
	 */
	
		Intent j = new Intent(this, Contrareloj_Finalizar.class);
		startActivity(j);
		
		finish();

		
	}

/****************************************************************************************************************************************************/
	
	private void guardarConfiguracion() {
			
			
		/*
		 * guardamos la configuracion que queramos en un archivo xml que se fuardar� en nuestro dispositivo
		 */
			   SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);   
			   SharedPreferences.Editor editor = prefs.edit();
			   
			   editor.putString(Variables.RECORD_GENERAL+"v2", var.num_score_cj+"");
			   editor.putBoolean(Variables.SONIDO_SWITCH, boolean_sonido);
			   editor.commit();
			  
			   //apagamos musica
			   
				
		}

	private void cargarConfiguracion() {
			// TODO Auto-generated method stub
			
		      SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);    
		      prefs.getString("Score", "Prueba lectura xml configuraci�n");
		      vibrar=prefs.getBoolean(Variables.VIBAR_SWITCH,true);
		      boolean_sonido=prefs.getBoolean(Variables.SONIDO_SWITCH,true);
		      nivel=prefs.getString(Variables.NIVEL_DIFICULTAD,Variables.FACIL);
		      
		      gestionar_sonido(boolean_sonido);

		}

/****************************************************************************************************************************************************/

	private void Comodines(int num_comodines_funcion) {
		
		/*
		 * Funci�n que nos gestiona el uso de los comodines: en primer lugar se crea una animacion tipo alpha, seguidamente testeamos mediante
		 * un if el numero de comodines que tenemos en este momento, en caso de que sea mayor que 1 y a la vez menor que 5 se gestionara los 
		 * ImagenViews de los comodines para que aparezcan los necesarios, a la vez se utilizara la animacion creada sobre el ultimo elemento 
		 * que hemos activado. En el caso de que lleguemos a los cinco comodines, se a�adir� 15 segundos al contador de tiempo y se 
		 * ejecutara una animacion para todos los comodines.
		 */
		
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
		anim.reset();
		
		
		if(num_comodines_funcion>=1 && num_comodines_funcion<5){
			
			for(int i=0;i<num_comodines_funcion;i++){
				var.comodines_cj[i].setImageResource(R.drawable.comodinon);
			}
			
			for(int i=num_comodines_funcion;i<var.comodines_cj.length;i++){
				var.comodines_cj[i].setImageResource(R.drawable.comodinoff);
			}
			
		
			
			var.comodines_cj[num_comodines_funcion-1].startAnimation(anim);
		}else{
			
			if(num_comodines_funcion==5){
				
				//TODO Crear animacion que rote 360� para cuando se consigue el comodin
				var.comodines_cj[4].setImageResource(R.drawable.comodinon);
				var.lincomodin_cj.setAnimation(anim);
				
				for(int i=0;i<var.comodines_cj.length;i++){
					var.comodines_cj[i].setImageResource(R.drawable.comodinoff);
				}
				
				var.num_comodines_cj=0;
				
				tiempo.cancel();
				segundos=segundos+30;
				
				if(segundos>59){
					segundos=segundos-60;
					minutos++;
				}
				iniciar_temporizador(milis_timer+30000, Variables.ESCALADO_TIEMPO_CONTRARELOJ_CJ);
		
			}
			
				for(int i=0;i<var.comodines_cj.length;i++){
				var.comodines_cj[i].setImageResource(R.drawable.comodinoff);
				}
			}
			//TODO aumentar tiempo
			
			
			
		}
		
		
/****************************************************************************************************************************************************/
	
	private void generarjuego() {
		/*
		 * Funcion que genera un juego nuevo, es decir una nueva ronda, para ello llamamos a las dos funciones que se observan 
		 */

		dibujar_emoticonosaleatorios();
		
		generar_frases_aleatorias();	
		
	}
	
/****************************************************************************************************************************************************/
	
	private void generar_frases_aleatorias() {
		
		/*
		 * Inserta en los botones textos del array peliculas aleatoriamente.
		 */
		
		//Insertar en los botones el texto de cada componente
		
				do{
					var.vector_peliculas_cj = (Vector<?>)tools.Frases_Aleatorias(Variables.NUMERO_BOTONES,var.indice_codigo_cj,nivel).clone();
					
				}while (tools.frasesduplicadas(var.vector_peliculas_cj)==false);
				
				
				
				for(int i=0;i<var.botones_cj.length;i++){
					
					String pelicula = (String) getString((Integer) var.vector_peliculas_cj.get(i));
					var.botones_cj[i].setText(pelicula);
				}
				
			//	texto.setText(var.vector_peliculas.size()+"");
	}
	
/****************************************************************************************************************************************************/
	
	private void dibujar_emoticonosaleatorios() {
		
		/*
		 * Dibuja aleatoriamente en la pantalla la combinacion de los emoticonos que queremos formar, para que aparezca la imagen de una pelicula
		 */
		int numeroaleatorio=0;
		String a="";
		
		if(nivel.equals(Variables.FACIL)){
			do{
				numeroaleatorio = tools.Numero_Aleatorio(datos.titulos_peliculas_id_facil.length); //generamos numero aleatorio
			}while(tools.Numero_Aleatorio_No_Usado_con_Anterioridad(numeroaleatorio,vindices_usados)==false);
			
			if(vindices_usados.size() == datos.titulos_peliculas_id_facil.length-1){
				vindices_usados.clear();
				
			}else{
				vindices_usados.add(numeroaleatorio);
			}
			
			var.indice_codigo_cj=numeroaleatorio; //copiamos el resultado en esa variable 
			
			a = datos.indice_peliculas_facil[numeroaleatorio];	//creamos un string y guardamos el indice de las pelicula aleatoria en el
			
			try{
				var.vector_iconos_cj=(Vector<?>)tools.DescifrarIndicePeliculas(a).clone();	//igualamos el vector_iconos al resultado de esta funcion, con esto obtenemos los enteros de las peliculas 
			}catch(ArrayIndexOutOfBoundsException e){
				
			}
			
			//inserta en los ImageView los componentes de cada vector.
			
			for(int i=0;i<var.vector_iconos_cj.size();i++){
				
				int x=0;
				x=(Integer)var.vector_iconos_cj.get(i);
				var.imagenes_cj[i].setImageResource(datos.paquete_imagenes_facil[x]);
			}
		}
		
		if(nivel.equals(Variables.MEDIO)){
			do{
				numeroaleatorio = tools.Numero_Aleatorio(datos.titulos_peliculas_id_medio.length); //generamos numero aleatorio
			}while(tools.Numero_Aleatorio_No_Usado_con_Anterioridad(numeroaleatorio,vindices_usados)==false);
			
			if(vindices_usados.size() == datos.titulos_peliculas_id_medio.length-1){
				vindices_usados.clear();
				
			}else{
				vindices_usados.add(numeroaleatorio);
			}
			
			var.indice_codigo_cj=numeroaleatorio; //copiamos el resultado en esa variable 
			
			a = datos.indice_peliculas_medio[numeroaleatorio];	//creamos un string y guardamos el indice de las pelicula aleatoria en el
			
			try{
				var.vector_iconos_cj=(Vector<?>)tools.DescifrarIndicePeliculas(a).clone();	//igualamos el vector_iconos al resultado de esta funcion, con esto obtenemos los enteros de las peliculas 
			}catch(ArrayIndexOutOfBoundsException e){
				
			}
			
			//inserta en los ImageView los componentes de cada vector.
			
			for(int i=0;i<var.vector_iconos_cj.size();i++){
				
				int x=0;
				x=(Integer)var.vector_iconos_cj.get(i);
				var.imagenes_cj[i].setImageResource(datos.paquete_imagenes_medio[x]);
			}
		}
		
		if(nivel.equals(Variables.DIFICIL)){
			do{
				numeroaleatorio = tools.Numero_Aleatorio(datos.titulos_peliculas_id_dificil.length); //generamos numero aleatorio
			}while(tools.Numero_Aleatorio_No_Usado_con_Anterioridad(numeroaleatorio,vindices_usados)==false);
			
			if(vindices_usados.size() == datos.titulos_peliculas_id_dificil.length-1){
				vindices_usados.clear();
				
			}else{
				vindices_usados.add(numeroaleatorio);
			}
			
			var.indice_codigo_cj=numeroaleatorio; //copiamos el resultado en esa variable 
			
			a = datos.indice_peliculas_dificil[numeroaleatorio];	//creamos un string y guardamos el indice de las pelicula aleatoria en el
			
			try{
				var.vector_iconos_cj=(Vector<?>)tools.DescifrarIndicePeliculas(a).clone();	//igualamos el vector_iconos al resultado de esta funcion, con esto obtenemos los enteros de las peliculas 
			}catch(ArrayIndexOutOfBoundsException e){
				
			}
			
			//inserta en los ImageView los componentes de cada vector.
			
			for(int i=0;i<var.vector_iconos_cj.size();i++){
				
				int x=0;
				x=(Integer)var.vector_iconos_cj.get(i);
				var.imagenes_cj[i].setImageResource(datos.paquete_imagenes_dificil[x]);
			}
		}
		

			

		
		//pone elementos vacios donde no hay nada
		
		for(int j=var.vector_iconos_cj.size();j<var.imagenes_cj.length;j++){
			
			var.imagenes_cj[j].setImageResource(R.drawable.vacio);
		}
		
		Random ran = new Random();
		int tras1= ran.nextInt(datos.animaciones.length);
		int tras2= ran.nextInt(datos.animaciones.length);
		
		Animation trasladar1;
		Animation trasladar2;
		
		trasladar1 = AnimationUtils.loadAnimation(this,datos.animaciones[tras1]);
		trasladar1.reset();
		trasladar2 = AnimationUtils.loadAnimation(this,datos.animaciones[tras2]);
		trasladar2.reset();
	
		var.lin_imagen_cj.startAnimation(trasladar1);


		
	}
	
/****************************************************************************************************************************************************/

	private void botones_default() {
		
		//Ponemos la imagen de los botones por defecto
		
        for(int i=0;i<var.botones_cj.length;i++){
        	var.botones_cj[i].setBackgroundResource(R.drawable.botonesdefault);
        }
	}
	
/****************************************************************************************************************************************************/
	
	private void chequear_respuesta(int i) {
		
		//Chequeamos y se nos representa si es correcta o no, para ello, se a�ade una imagen de fondo.
		
		
		// guardamos en un String el titulo de la pelicula
		
		int pelicula_seleccionada = (Integer)var.vector_peliculas_cj.get(i);
		
		//guardamos en una variable entera el indice del codigo que hay en pantalla
		
		if(nivel.equals(Variables.FACIL)){
			
			for(int j=0;j<datos.titulos_peliculas_id_facil.length;j++){
				
				if(pelicula_seleccionada==datos.titulos_peliculas_id_facil[j]){
					var.indice_pelicula_cj=j;
				}
				
			}
		}
		
		if(nivel.equals(Variables.MEDIO)){
			
			for(int j=0;j<datos.titulos_peliculas_id_medio.length;j++){
				
				if(pelicula_seleccionada==datos.titulos_peliculas_id_medio[j]){
					var.indice_pelicula_cj=j;
				}
				
			}
		}
		
		if(nivel.equals(Variables.DIFICIL)){
			
			for(int j=0;j<datos.titulos_peliculas_id_dificil.length;j++){
				
				if(pelicula_seleccionada==datos.titulos_peliculas_id_dificil[j]){
					var.indice_pelicula_cj=j;
				}
				
			}
		}

					
		//llamamos a esta funcion que nos devolvera un true o un false 
		
		if(tools.Comprobar_Si_Dos_Numeros_Son_Iguales(var.indice_pelicula_cj,var.indice_codigo_cj)==true){
			
			var.botones_cj[i].setBackgroundResource(R.drawable.botonon);
			
			//Generar_animacion_score(true,i);

			var.num_score_cj=var.num_score_cj+Variables.ACIERTO_CJ;
			var.tscore_cj.setText(getString(R.string.score)+ " " +var.num_score_cj);
			
			var.acierto_cj++;
			
			var.num_comodines_cj++;
			
			if(boolean_sonido==true){
				snd.play(sonido_acierto, this);
			}
			
			
		}else{
			
			var.botones_cj[i].setBackgroundResource(R.drawable.botononoff);
			
			//Generar_animacion_score(false,i);

			
			
			var.num_score_cj = var.num_score_cj + Variables.FALLO_CJ;
			
			if(var.num_score_cj<0){
				
				var.num_score_cj=0;
			}
			
			var.tscore_cj.setText(getString(R.string.score) + " " +var.num_score_cj);
			
			
			var.fallos_cj++;
			
			
			var.num_comodines_cj=0;
			
			if(vibrar==true){
				
				Vibrator v = (Vibrator) getSystemService(getApplicationContext().VIBRATOR_SERVICE);
				// Vibrar durante 150 milisegundos
				v.vibrate(150);
				}
			
			
			if(boolean_sonido==true){
				snd.play(sonido_fallo, this);
			}
			
		}
		
	}

private void Generar_animacion_score(boolean b,int i) {
	
	if(b==true){
		var.imagenes_score[i].setBackgroundResource(R.drawable.puntos500);
	}else{
		var.imagenes_score[i].setBackgroundResource(R.drawable.puntos100);
	}
	
	
	score = AnimationUtils.loadAnimation(this,R.anim.score);
	score.reset();
	score.setAnimationListener(this);
	var.imagenes_score[i].setAnimation(score);
	
}
/****************************************************************************************************************************************************/
	
	private void iniciar_temporizador(long tiempo_temp,long escalado_temp) {
		
		tiempo = new Tiempo(tiempo_temp, escalado_temp);
		tiempo.start();
		
	}
	
/****************************************************************************************************************************************************/

	class Tiempo extends CountDownTimer{
		
		/*
		 * Clase que nos genera un CountDownTimer, Nota: buscar otra forma degenerar una cuenta regresiva.
		 */

		/*
		 * la variable millisInfuture Idica el tiempo en milis de la cuenta regresiva, la variable
		 * countDownInterval indica como quiere que contemos el tiempo, de 1 en 1 o de x en x
		 */
		public Tiempo(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			
		
		}
		



		@Override
		public void onFinish() {
			//Al terminar la cuenta
			
			//iniciamos la actividad final 
			
			juego_finalizado=true;
			iniciar_Activity_finish();
			this.cancel();
			
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// Accion que realizamos en cada preescalado
			
			/*
			 * Para poder gestionar bien el tiempo, hacemos una cosa, guardamos en una variable entera el tiempo que falta para que acabe el timer. en cada onTick 
			 * restamos uno a los segundo y con un par de if almacenamos en todo momento los segundo y minutos que tenemos, ademas se gestiona con otros dos if 
			 * si hay que escribir un cero detras del digito o no. Para saber si hemos terminado, es decir, si hemos llegado a cero segundos, chequeamos en todo momento
			 * si la variable segundos y minutos es cero, en caso afirmativo llamamos a la funcion onFinish(); la cual llama a la activity final.
			 */
			
			milis_timer= millisUntilFinished;
			
			String seg="";
			String min="";
			
			
			segundos--;
			
			
			if(segundos<0){
				segundos=59;
				minutos--;
				
			}
			
			
			if(segundos>=0 && segundos <=9){
				seg="0"+segundos;
			}else{
				seg=segundos+"";
			}
			
			if(minutos>=0 && minutos <=9){
				min="0"+minutos;
			}else{
				min=minutos+"";
			}
			
			
	
			
			if(minutos==0 && segundos==0){
				
				tiempo_acabado=true;
				
				minutos=0;
				segundos=0;
					
				var.ttime_cj.setText(min + ":" + seg);
				
				this.onFinish();
			
			}
			
			if(tiempo_acabado==false){
				var.ttime_cj.setText(min + ":" + seg);
			}
			
			if(minutos==0 && segundos <=10){
			
				var.ttime_cj.setTextColor(Color.RED);
				
			}
		}
		
	}
	
/****************************************************************************************************************************************************/	
	public void crear_Activity_Pausa() {
		
		
	}
	
/****************************************************************************************************************************************************/
	private void Generar_Animacion() {
		
		
		/*
		 * generamos la animacion inicial, para ello creamos una animacion y la gestionamos a traves de la interfaz onAnimationListener
		 */
		

		
		new AnimationUtils();
		anim1 = AnimationUtils.loadAnimation(this, R.anim.animacion_numeros);
		anim1.reset();
		anim1.setAnimationListener(this);
		new AnimationUtils();
		anim2 = AnimationUtils.loadAnimation(this, R.anim.animacion_numeros);
		anim2.reset();
		anim2.setAnimationListener(this);
		new AnimationUtils();
		anim3 = AnimationUtils.loadAnimation(this, R.anim.animacion_numeros);
		anim3.reset();
		anim3.setAnimationListener(this);
		
		imagen_animacion.setImageResource(R.drawable.numero3);
		imagen_animacion.startAnimation(anim1);
		
		gestionar_sonido(boolean_sonido);
		

	}
	
	@Override	
	public void onAnimationEnd(Animation animation) {
		
		
			/*
			 * aqui generamos la animacion del principio, para ello se proporciona una variable que va cambiando y con un poco de ma�a y suerte se hace todo XD!!!
			 */
		if(animation.equals(anim1)){
			
			imagen_animacion.setImageResource(R.drawable.numero2);
			imagen_animacion.startAnimation(anim2);	
			
			
		}
		
		if(animation.equals(anim2)){
			
			imagen_animacion.setImageResource(R.drawable.numero1);
			imagen_animacion.startAnimation(anim3);	
			
			
		}
		
		if(animation.equals(anim3)){
			

			imagen_animacion.setImageResource(R.drawable.vacio);
			
			generarjuego();
			
			iniciarcomponentes();
			
			iniciar_temporizador(Variables.TIEMPO_CONTRARELOJ_CJ, Variables.ESCALADO_TIEMPO_CONTRARELOJ_CJ);
			
		}
			
					
		
		
		//if(animation.equals(score)){
			//for(int i=0;i<var.imagenes_score.length;i++){
				
				//var.imagenes_cj[i].setImageResource(R.drawable.vacio);
				
		//	}
		}
	

	@Override
	public void onAnimationRepeat(Animation animation) {
		
		
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		
			
		
	}
/****************************************************************************************************************************************************/
	
	private void iniciarcomponentes() {
		
		
		/*
		 * una vez que ha terminado la animacion se llama a esta funcion que consiste en poner sobre la pantalla todos los componentes de forma animada;
		 */
		
		Random x = new Random(); 
		int y;
		
		for(int i=0;i<var.botones_cj.length;i++){
			y=x.nextInt(datos.animaciones.length);
			anime=AnimationUtils.loadAnimation(this, datos.animaciones[y]);
			anime.reset();

			var.botones_cj[i].setBackgroundResource(R.drawable.botonesdefault);
			var.botones_cj[i].setEnabled(true);
			var.botones_cj[i].setClickable(true);
			var.botones_cj[i].startAnimation(anime);
		}
		
		for(int i=0;i<var.comodines_cj.length;i++){
		
			y=x.nextInt(datos.animaciones.length);
			anime=AnimationUtils.loadAnimation(this, datos.animaciones[y]);
		anime.reset();
			var.comodines_cj[i].setImageResource(R.drawable.comodinoff);
			var.comodines_cj[i].startAnimation(anime);
		}
		
		y=x.nextInt(datos.animaciones.length);
		anime=AnimationUtils.loadAnimation(this, datos.animaciones[y]);
		anime.reset();
		var.tscore_cj.setText(getString(R.string.score)+" 0");
		var.tscore_cj.startAnimation(anime);
		
		y=x.nextInt(datos.animaciones.length);
		anime=AnimationUtils.loadAnimation(this, datos.animaciones[y]);
		anime.reset();
		
		anime.setAnimationListener(this);
		
		var.ttime_cj.startAnimation(anime);
		

		
		bpausa.setBackgroundResource(R.drawable.pause);
		bpausa.setClickable(true);
		bpausa.setEnabled(true);
		bpausa.startAnimation(anime);
		
		bsonido.setBackgroundResource(R.drawable.sonidoon);
		bsonido.setClickable(true);
		bsonido.setEnabled(true);
		bsonido.startAnimation(anime);
		
		
		animacion_on=false;
		
		lin_general.setVisibility(View.VISIBLE);
		
		gestionar_sonido(boolean_sonido);

	}

	private void borrar_componentes() {
		
		
		/*
		 * esta funcion es llamada antes de generar la animacion y consiste en ocultar todos los views de la pantalla
		 */
		
//		for(int i=0;i<var.botones_cj.length;i++){
//			var.botones_cj[i].setBackgroundResource(R.drawable.vacio);
//			var.botones_cj[i].setClickable(false);
//			var.botones_cj[i].setEnabled(false);
//			var.botones_cj[i].setText("");
//		}
//		
//		for(int i=0;i<var.comodines_cj.length;i++){
//			var.comodines_cj[i].setImageResource(R.drawable.vacio);
//			
//		}
//		
//		for(int i=0;i<var.imagenes_cj.length;i++){
//			var.imagenes_cj[i].setImageResource(R.drawable.vacio);
//			
//		}
//		
//		var.btest_cj.setBackgroundResource(R.drawable.vacio);
//		var.btest_cj.setClickable(false);
//		var.btest_cj.setEnabled(false);
//		var.btest_cj.setText("");
//		
//		bpausa.setBackgroundResource(R.drawable.vacio);
//		bpausa.setClickable(false);
//		bpausa.setEnabled(false);
//		bpausa.setText("");
//		
//		var.tscore_cj.setText("");
//		var.ttime_cj.setText("");
//		animacion_on=true;
		
		lin_general.setVisibility(View.INVISIBLE);
		
	}
	
	private void pausar_juego() {
			
		boolean_pausa=true;
		
//		vdatos_pausa.clear();
//		
//		for(int i=0; i<var.botones_cj.length;i++){
//			vdatos_pausa.add(var.botones_cj[i].getText().toString());
//		}
//		for(int i=0; i<var.imagenes_cj.length;i++){
//			vdatos_pausa.add(var.imagenes_cj[i].getDrawable());
//		}
//		
//
//		
//			
//		
//		borrar_componentes();
		
		lin_general.setVisibility(View.INVISIBLE);
		tiempo.cancel();
		iniciar_animacion_pausa();
		
		}


/****************************************************************************************************************************************************/
		private void iniciar_animacion_pausa() {
		
			



			LayoutParams params=new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT);
			
			LayoutParams params2=new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			
			lin = new LinearLayout(this);
			
			lin.setOrientation(1);
			
			text = new TextView(this);
			TextView text2 = new TextView(this);
			
			bmenu = new Button(this);
			bvolver = new Button(this);
			
	        text.setTextColor(Color.WHITE);
	        Typeface type_letra =Typeface.createFromAsset(getAssets(), Variables.FUENTE_LETRA_PROGRAMA);
			text.setTypeface(type_letra);
			text.setText(getString(R.string.pausa));
			text.setTextSize(72);		
			text.setLayoutParams(params2);
			text.setGravity(Gravity.CENTER);
			

	        bmenu.setTextColor(Color.WHITE);
	        bmenu.setTypeface(type_letra);
			bmenu.setText(getString(R.string.menu));
			bmenu.setTextSize(72);		
			bmenu.setLayoutParams(params2);
			bmenu.setGravity(Gravity.CENTER);
			bmenu.setBackgroundResource(R.drawable.botonesdefault);
			bmenu.setVisibility(View.VISIBLE);
			bmenu.setOnClickListener(this);
			
	        bvolver.setTextColor(Color.WHITE);
	        bvolver.setTypeface(type_letra);
			bvolver.setText(getString(R.string.volver));
			bvolver.setTextSize(72);		
			bvolver.setLayoutParams(params2);
			bvolver.setGravity(Gravity.CENTER);
			bvolver.setBackgroundResource(R.drawable.botonesdefault);
			bvolver.setVisibility(View.VISIBLE);
			bvolver.setOnClickListener(this);
			
		
			lin.addView(text);
			lin.addView(bmenu);
			lin.addView(bvolver);
			lin.setGravity(Gravity.CENTER_HORIZONTAL);
			
			
			lin.setLayoutParams(params);
			this.addContentView(lin,params);
	}
		
		private void Reanudar_juego() {
			// TODO Auto-generated method stub
			
			boolean_pausa=false;
			
			
			iniciar_temporizador(milis_timer,Variables.ESCALADO_TIEMPO_CONTRARELOJ_CJ);
			segundos++;
			
//			for(int i=0; i<var.botones_cj.length;i++){
//				var.botones_cj[i].setText((String)vdatos_pausa.get(i));
//			}
//			for(int i=var.botones_cj.length; i<vdatos_pausa.size();i++){
//
//				var.imagenes_cj[i-var.botones_cj.length].setImageDrawable((Drawable)vdatos_pausa.get(i));
//			}
//			
//
//			
//			
//			
//			vdatos_pausa.clear();
//			
//			text.setText("");
//			lin.removeAllViews();
//			iniciarcomponentes();
//			Comodines(var.num_comodines_cj);
//			var.tscore_cj.setText(getString(R.string.score)+var.num_score_cj);
			
			text.setText("");
			bmenu.setVisibility(View.INVISIBLE);
			bvolver.setVisibility(View.INVISIBLE);
			lin_general.setVisibility(View.VISIBLE);
			
		}
		private void gestionar_sonido(Boolean sonido) {
			// TODO Auto-generated method stub
				if(sonido==false){
					bsonido.setBackgroundResource(R.drawable.sonidooff);
					mp.pause();
					
					
				}
				
				if(sonido==true){
					bsonido.setBackgroundResource(R.drawable.sonidoon);
					mp.start();
				}
		}
		
}


