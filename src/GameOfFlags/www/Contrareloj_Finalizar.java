package GameOfFlags.www;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
public class Contrareloj_Finalizar extends Activity implements OnClickListener,AnimationListener{
	
	Button bjugar_de_nuevo,bmenu,bF_submit_score;
	EditText nombre;
	TextView trecord,thighscore,tscore_hs,tsubmit;
	
	Animation anim1,anim2;
	
	LinearLayout lin_general;
	
	
	int score=0;
	int id_boton=0;
	
	Boolean animacion_en_progreso=true;
	
    Variables var = new Variables();
    Tools tools =new Tools(); 
    Datos datos=new Datos();
    
    Boolean boolean_valorar=true;
    
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    
    private static String ID_FB="412785915449880";
    private Facebook facebook;
    private AsyncFacebookRunner mAsyncRunner;
    
    String nivel="";
    
   // private SharedPreferences mPrefs;

	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contrareloj_finalizar);
  
        //tipo de letra de esta activity
        
        Typeface type_letra = Typeface.createFromAsset(getAssets(), Variables.FUENTE_LETRA_PROGRAMA);
        
        // a cada boton le ponemos el tipo de letra que queremos
        
        bjugar_de_nuevo = (Button) findViewById(R.id.bjugar_de_nuevo);
        bjugar_de_nuevo.setTypeface(type_letra);
        bjugar_de_nuevo.setOnClickListener(this);
        
        bmenu = (Button) findViewById(R.id.bcont_menu);
        bmenu.setTypeface(type_letra);
        bmenu.setOnClickListener(this);
        
        bF_submit_score = (Button) findViewById(R.id.bfacebook_submit_score);
        bF_submit_score.setOnClickListener(this);
        
        nombre=(EditText)findViewById(R.id.edit_nombre);
        nombre.setTypeface(type_letra);
        nombre.setText("");
        trecord=(TextView)findViewById(R.id.tfin_score);
        trecord.setTypeface(type_letra);
        thighscore=(TextView)findViewById(R.id.tfin_high_score);
        thighscore.setTypeface(type_letra);
        
        tsubmit=(TextView)findViewById(R.id.tsubmit);
        tsubmit.setTypeface(type_letra);
        tsubmit.setOnClickListener(this);
        
       
        tscore_hs=(TextView)findViewById(R.id.tfin_score_hs);
        tscore_hs.setTypeface(type_letra);
        
        lin_general=(LinearLayout)findViewById(R.id.lin_fin_gen);
        
        
       // tools.guardar_preferencia(var.NOMBRE_FICHERO_PREFERENCIAS_RECORDS, "SCORE", "500");
        
        
        

        
}
	

	






	@Override
	protected void onDestroy() {
		super.onDestroy();

	}
	
	@Override
	protected void onResume() {
		super.onResume();
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        cargarConfiguracion();
        iniciar_animacion_inicial();

	}
	
    @Override
    protected void onStart() {
      super.onStart();

    }    

	protected void onPause() {
		
		super.onPause();
		
	}
	
	protected void onStop() {
		
		super.onStop();
		
	}


	

	public boolean onKeyUp(int keyCode, KeyEvent event) {
		
		if(keyCode==KeyEvent.KEYCODE_HOME || keyCode==KeyEvent.KEYCODE_SEARCH){
			

			stopService(new Intent(this, MusicaDeFondo.class));
			finish();
			
		}
		
		if(keyCode==KeyEvent.KEYCODE_BACK){
			// do something on back.
			
			// do something on back.
			if(animacion_en_progreso==false){
				id_boton=R.id.bcont_menu;		
				iniciar_animacion_final();
			}	
			
			
		}
		
		return true;
		
	}
		


	public void onClick(View v) {
		
		if(v.getId()==R.id.bjugar_de_nuevo){
			
			if(animacion_en_progreso==false){
				id_boton=R.id.bjugar_de_nuevo;		
				iniciar_animacion_final();
			}
			
			

			
		}
		
		if(v.getId()==R.id.bcont_menu){
			
			if(animacion_en_progreso==false){
				id_boton=R.id.bcont_menu;
				iniciar_animacion_final();
			}

			
		}
		
		if(v.getId()==bF_submit_score.getId()){
			submit_Score();
		}
		
		if(v.getId()==tsubmit.getId()){
			submit_Score();
		}
		
	}
	
	private void guardarConfiguracion() {
		
		
		/*
		 * guardamos en las preferencias nuestro score, para que poder cargarlo en la activity de Records, a la vez si el valor
		 * que introducimos en nuestro edittext es nulo le damos como nombre por defecto Smiley.
		 */
	
		   prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
		   
		   editor = prefs.edit();
		   
		   if(score!=0){
			   
			   editor.putString(Variables.RECORD_GENERAL, score+"");
			   
			   if(nombre.getText().toString().equals("")){
				   editor.putString("Record_Nombre_General", "Smiley"); 
			   }else{
				   editor.putString("Record_Nombre_General", nombre.getText()+"");
			   }
			   
			   editor.putString(Variables.NOMBRE_TEXTEDIT_CJ_FINALIZAR,nombre.getText().toString());
			   
			   editor.commit();
			   
		   }
	
		  
	}




	private void cargarConfiguracion() {
	
	
	/*
	 * aqui cargamos el score, asi como el high score y lo mostramos en pantalla
	 */
		
	
	  int h_score=0;
		
	  SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
	  
	  boolean_valorar= prefs.getBoolean(Variables.BOOLEAN_VALORAR_APP, true);
	  
	  try{
		  score=Integer.parseInt(prefs.getString(Variables.RECORD_GENERAL+"v2", "0"));
		  h_score=Integer.parseInt(prefs.getString("Record1", "0"));		  
	  }catch(NumberFormatException e){
		  
	  }
	  
	  if(h_score==0){
		  tscore_hs.setText(getText(R.string.mensaje_animo_001));
		  tscore_hs.setTextColor(Color.WHITE);
		  thighscore.setText(getString(R.string.high_score));
		  thighscore.setTextColor(Color.WHITE);
	  }
	  
	  if(score>=h_score){
		  
		  tscore_hs.setText(score+"");
		  tscore_hs.setTextColor(Color.GREEN);
		  thighscore.setText(getString(R.string.Nuevo_record));
		  thighscore.setTextColor(Color.GREEN);
	  }else{
		  tscore_hs.setText(h_score+"");
	  }
	  


	
	  if(score==0){
		  trecord.setText(getString(R.string.score) + " " + getString(R.string.mensaje_animo_001)); 
	  }else{
		  trecord.setText(getString(R.string.score) + score); 
	  }
	
	  String a=prefs.getString(Variables.NOMBRE_TEXTEDIT_CJ_FINALIZAR,"null");
	
	  if(a.equals("null")){
		  nombre.setHint(getString(R.string.introducir_nombre));
	  }else{
		  nombre.setText(a.toString());
	  }
	  
	  nivel=prefs.getString(Variables.NIVEL_DIFICULTAD,Variables.FACIL);
	
	 }




	private void iniciar_animacion_inicial() {
		
		
		//iniciamos la animacion inicial
		
		animacion_en_progreso=true;
		
		anim1=AnimationUtils.loadAnimation(this, R.anim.translate_arriba_centro);
		anim1.reset();
		anim1.setAnimationListener(this);
		
		lin_general.setAnimation(anim1);
		
	}

	private void iniciar_animacion_final() {
		
		//llamamos a esta funcion cuando queremos terminar con la activity
		
		animacion_en_progreso=true;
		
		anim2=AnimationUtils.loadAnimation(this, R.anim.translate_centro_abajo);
		anim2.reset();
		anim2.setAnimationListener(this);
		
		lin_general.setAnimation(anim2);
		
		
	}









	private void borrar_pantalla() {
		
		//borramos todos los componentes de la pantalla
		
//		thighscore.setText("");
//		thighscore.setBackgroundResource(R.drawable.vacio);
//		trecord.setText("");
//		trecord.setBackgroundResource(R.drawable.vacio);
//		tscore_hs.setText("");
//		tscore_hs.setBackgroundResource(R.drawable.vacio);
//		
//		bjugar_de_nuevo.setBackgroundResource(R.drawable.vacio);
//		bmenu.setBackgroundResource(R.drawable.vacio);
//		bjugar_de_nuevo.setText("");
//		bmenu.setText("");
//		nombre.setBackgroundResource(R.drawable.vacio);
//		nombre.setText("");
//		nombre.setHint("");
		
		lin_general.setVisibility(View.INVISIBLE);
		
	}









	@Override
	public void onAnimationEnd(Animation animation) {
		
		
		if(animation.equals(anim1)){
			animacion_en_progreso=false;
			
			if(boolean_valorar==true){
				Abrir_Ventana_Valorar();
			}
			
		}
		
		if(animation.equals(anim2)){
			
			guardarConfiguracion();
			
			borrar_pantalla();
			
			animacion_en_progreso=false;
			
			
			if(id_boton==R.id.bjugar_de_nuevo){
				
				Intent j = new Intent(this, JuegoContrareloj.class);
				startActivity(j);
				
				finish();
				
			}
			if(id_boton==R.id.bcont_menu){
				
				Intent j = new Intent(this, Menu_Principal.class);
				startActivity(j);	
				finish();
			}

		}
	}




	@Override
	public void onAnimationRepeat(Animation animation) {
		
	}




	@Override
	public void onAnimationStart(Animation animation) {
		
	}

	
	
	private void submit_Score() {
		// TODO Auto-generated method stub
		
			facebook = new Facebook(ID_FB);
	        mAsyncRunner = new AsyncFacebookRunner(facebook);

	        
	        /*
	         * Get existing access_token if any
	         */
//	        mPrefs = getPreferences(MODE_PRIVATE);
//	        String access_token = mPrefs.getString("access_token", null);
//	        long expires = mPrefs.getLong("access_expires", 0);
//	        if(access_token != null) {
//	        	
//	            facebook.setAccessToken(access_token);
//	            
//	            mensaje_En_El_Muro();
//	            
//	        }
//	        if(expires != 0) {
//	            facebook.setAccessExpires(expires);
//	        }
	        
	        /*
	         * Only call authorize if the access_token has expired.
	         */
	        
	        
	        
	        if(!facebook.isSessionValid()) {

	            facebook.authorize(this, new String[] {}, new DialogListener() {
	                @Override
	                public void onComplete(Bundle values) {
//	                    SharedPreferences.Editor editor = mPrefs.edit();
//	                    editor.putString("access_token", facebook.getAccessToken());
//	                    editor.putLong("access_expires", facebook.getAccessExpires());
//	                    editor.commit();

	            		Toast mensaje= Toast.makeText(Contrareloj_Finalizar.this,getString(R.string.autorizado), Toast.LENGTH_SHORT);
	            		mensaje.show();
	            		
	        			mensaje_En_El_Muro();
	            		
	        			

	                }
	    


					@Override
	                public void onFacebookError(FacebookError error) {
	                	
	                	Toast mensaje= Toast.makeText(Contrareloj_Finalizar.this,getString(R.string.facebook_error), Toast.LENGTH_SHORT);
	            		mensaje.show();
	                }
	                
            		
	                
	                @Override
	                public void onError(DialogError e) {
	                	
	                	Toast mensaje= Toast.makeText(Contrareloj_Finalizar.this,getString(R.string.se_ha_producido_un_error), Toast.LENGTH_SHORT);
	            		mensaje.show();
	                }
	                
	                
	                @Override
	                public void onCancel() {}
	            });
	        }
	       
	        
	       
	    }
	
    private void mensaje_En_El_Muro() {
		// TODO Auto-generated method stub
    	Bundle params = new Bundle();
		
		params.putString("description", getString(R.string.descripcion_juego));
	    params.putString("name", getString(R.string.app_name));
	    params.putString("caption", getString(R.string.caption) + getString(R.string.Nivel)+": "+ nivel +"          " +getString(R.string.score)+"" + score);
	    params.putString("picture", "http://www.tapizadosbeyra.com/moviesmile/icono_juegov3.png");
	    params.putString("link", Variables.ENLACE_PLAY_GOOGLE);
	    facebook.dialog(Contrareloj_Finalizar.this, "feed", params, new SampleDialogListener());
	}
	
    private void Abrir_Ventana_Valorar() {
		// TODO Auto-generated method stub
		 final AlertDialog.Builder alertaCompuesta = new AlertDialog.Builder(this);
		 
		 CheckBox check = new CheckBox(this);
		 TextView t = new TextView(this);
		 LinearLayout lin_alert = new LinearLayout(this);
		 
		 //LayoutParams params=new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		 
		 
		 lin_alert.setOrientation(0);
		 
		 t.setText(getText(R.string.No_mostrar_mensaje));
		 
		 check.setChecked(false);
		 
		 lin_alert.setGravity(Gravity.CENTER);
		 lin_alert.addView(t);
		 lin_alert.addView(check);
		 
		 check.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
					
					prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
					editor = prefs.edit();
					editor.putBoolean(Variables.BOOLEAN_VALORAR_APP,!isChecked);   
					editor.commit();				
			}
		});
		 
		 
		 alertaCompuesta.setTitle(getString(R.string.ValorarAplicacion));
		 alertaCompuesta.setView(lin_alert);
		 alertaCompuesta.setMessage(getString(R.string.Mensaje_Valorar_Aplicacion));
		 alertaCompuesta.setPositiveButton(getString(R.string.puntuar),
				 
		 new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int which) {
		       Intent i = new Intent("android.intent.action.VIEW", Uri.parse(Variables.ENLACE_PLAY_GOOGLE));
		       startActivity(i);
		 }
		 });
		 alertaCompuesta.setNegativeButton(getString(R.string.mas_tarde),
		 new DialogInterface.OnClickListener() {
		 public void onClick(DialogInterface dialog, int which) {
		 }
		 });
		 
		 
		 
		 alertaCompuesta.show();
	}









	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        facebook.authorizeCallback(requestCode, resultCode, data);
    }




	
    public class SampleDialogListener extends BaseDialogListener {

        public void onComplete(Bundle values) {
        	Toast mensaje= Toast.makeText(Contrareloj_Finalizar.this,getString(R.string.enviado), Toast.LENGTH_SHORT);
    		mensaje.show();
        }
    }
    
 	
}
