package GameOfFlags.www;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Menu_Principal extends Activity implements OnClickListener,AnimationListener{


	 TextView tcontrareloj,trecord,tconfig,ttitulo;
	 
	 ImageView imagen,ima_punto1,ima_punto2,ima_punto3;
	 LinearLayout lin1,lin2,lin3,lin4;
	 
	 Button bfacebook,bsound;
	 
	 Animation anim1,anim2,anim3,anim4,anim4v2,anim5,anim6,anim7,anim8;
	 
		Variables var;
		Datos datos;

		Tools tools;
	 
		int id_seleccionada=0;
		
		MediaPlayer mp;
		
		int musica_transicion=0;
		
		SoundManager snd;
		
		Boolean sonido=true;
		
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.main);
	        
	       // requestWindowFeature(Window.FEATURE_NO_TITLE);
	        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        
	        //requestWindowFeature(Window.FEATURE_NO_TITLE);
	       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        
			var = new Variables();
			Datos datos= new Datos();

			Tools tools= new Tools();
	        
			Typeface type_letra = Typeface.createFromAsset(getAssets(), var.FUENTE_LETRA_PROGRAMA+"");
	        
	        tcontrareloj = (TextView) findViewById(R.id.text_prin_contrareloj);
	        trecord = (TextView) findViewById(R.id.text_prin_record);
	        tconfig = (TextView) findViewById(R.id.text_prin_config);
	        ttitulo = (TextView) findViewById(R.id.ttitulo_main);
	        
	        tcontrareloj.setTypeface(type_letra);
	        trecord.setTypeface(type_letra);
	        tconfig.setTypeface(type_letra);
	        ttitulo.setTypeface(type_letra);
	        
	        	        
	        tcontrareloj.setOnClickListener(this);
	        trecord.setOnClickListener(this);
	        tconfig.setOnClickListener(this);
	        
	        tcontrareloj.setText("");
	        trecord.setText("");
	        tconfig.setText("");
	       
	        
	        imagen=(ImageView)findViewById(R.id.imageView1);
	        imagen.setImageResource(R.drawable.vacio);
	        ima_punto1=(ImageView)findViewById(R.id.icono1);
	        ima_punto3=(ImageView)findViewById(R.id.icono2);
	        ima_punto2=(ImageView)findViewById(R.id.icono3);
	        
	        lin1=(LinearLayout)findViewById(R.id.lin_main_1);
	        lin2=(LinearLayout)findViewById(R.id.lin_main_2);
	        lin3=(LinearLayout)findViewById(R.id.lin_main_3);
	        lin4=(LinearLayout)findViewById(R.id.lin_main_4);
	         
	    	bfacebook=(Button)findViewById(R.id.bfacebook_main);
	    	bsound=(Button)findViewById(R.id.bsonido_main);
	    	
	    	bfacebook.setOnClickListener(this);
	    	bsound.setOnClickListener(this);
	        
	        snd= new SoundManager(this);			
			this.setVolumeControlStream(AudioManager.STREAM_MUSIC); 
	    	musica_transicion= snd.load(R.raw.sonido_acierto);

		    
			mp= MediaPlayer.create(this, R.raw.main_theme);
			mp.setLooping(true);


	        
	        //startService(new Intent(this, MusicaDeFondo.class));

	        
	 }
	 
	 
		private void iniciar_animacion_inicial() {
		
			anim1=AnimationUtils.loadAnimation(this,R.anim.translate_abajo_centro);
	        anim1.reset();
	        anim1.setAnimationListener(this);
	        
	        anim2=AnimationUtils.loadAnimation(this, R.anim.rotar1);
	        anim2.reset();
	        anim2.setAnimationListener(this);
	        
	        anim3=AnimationUtils.loadAnimation(this, R.anim.translate_arriba_centro);
	        anim3.reset();
	        anim3.setAnimationListener(this);
	        
	        anim4=AnimationUtils.loadAnimation(this, R.anim.translate_der_centro);
	        anim4.reset();
	        anim4.setAnimationListener(this);
	        
	        anim4v2=AnimationUtils.loadAnimation(this, R.anim.translate_izq_centro);
	        anim4v2.reset();
	        anim4v2.setAnimationListener(this);
	        	        
	        lin1.startAnimation(anim1);
	        lin2.startAnimation(anim1);
	        lin3.startAnimation(anim1);
	        lin4.startAnimation(anim1);
	        
	        ttitulo.setText("");
	        
	        
	        ima_punto1.startAnimation(anim2);
	        ima_punto2.startAnimation(anim2);
	        ima_punto3.startAnimation(anim2);
	        
	}


		private void iniciar_animacion_final() {
			
			
	        anim5=AnimationUtils.loadAnimation(this, R.anim.translate_centro_der);
	        anim5.reset();
	        anim5.setAnimationListener(this);
	        
	        anim6=AnimationUtils.loadAnimation(this, R.anim.translate_centro_abajo);
	        anim6.reset();
	        anim6.setAnimationListener(this);
	        
	        anim7=AnimationUtils.loadAnimation(this, R.anim.translate_centro_arriba);
	        anim7.reset();
	        anim7.setAnimationListener(this);
	        
	        anim8=AnimationUtils.loadAnimation(this, R.anim.translate_centro_izq);
	        anim8.reset();
	        anim8.setAnimationListener(this);
	        
	        imagen.startAnimation(anim5);
	        
	        gestionar_sonido(sonido);
	        
			
		}
		
//		protected void onSaveInstanceState(Bundle bun){
//			super.onSaveInstanceState(bun);
//			if(mp!=null){
//				bun.putInt("posicion", mp.getCurrentPosition());
//			}
//		}
//		
//		protected void onRestoreInstanceState(Bundle bun){
//			super.onRestoreInstanceState(bun);
//			if(mp!=null){
//				mp.seekTo(bun.getInt("posicion"));
//			}
//		}
		
		
		
		@Override
	    public void onDestroy()
	    {
			
			
			
//			//matar aplicacion
//			
//			android.os.Process.killProcess(android.os.Process.myPid());
//			 
//			//borrar memoria ram
//			
//		    File[] dir = this.getCacheDir().listFiles();
//		    if(dir != null){
//		        for (File f : dir){
//		            f.delete();
//		        }
//		       }

			
	    	
	    	  //Evento "Cerrar aplicación" guardardamos los datos que queramos en un fichero xml 
	      super.onDestroy();
	      
	      
	      
	     // stopService(new Intent(this, MusicaDeFondo.class));
	    } 

	    @Override
	    protected void onStart()
	    {
	    	//evento "Abrir aplicación" leemos configuración del fichero xml
	    	
	      super.onStart();
	     
	    }    
	    
	    @Override
	    protected void onResume()
	    {
	    	//evento "Abrir aplicación" leemos configuración del fichero xml
	    	
	      super.onResume();
	      
	      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	      
	      this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
	      
	      cargar_configuracion();
	      
		  iniciar_animacion_inicial();
		      
		 

	      
	    }   



		protected void onPause() {
			
			super.onPause();
			mp.release();
			guardarconfiguracion();
			finish();
		}

	 
		private void guardarconfiguracion() {
			// TODO Auto-generated method stub
			
			   SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
			   
			   SharedPreferences.Editor editor = prefs.edit();
//			   
//			   flujo_musica=mp.getCurrentPosition();
//			   
			   editor.putBoolean(Variables.SONIDO_SWITCH,sonido);
			   editor.commit();
				   
			   }
		

		private void cargar_configuracion() {
			// TODO Auto-generated method stub
			
			SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
			sonido=prefs.getBoolean(Variables.SONIDO_SWITCH, true);
			
//			if(flujo_musica!=0){
//				mp.seekTo(flujo_musica);
//
//				mp.start();
//			}else{
//				mp.start();
//			}
			
//			Toast mensaje= Toast.makeText(this, ""+flujo_musica, Toast.LENGTH_SHORT);
//			mensaje.show();
			
//			mp.seekTo(flujo_musica);
			mp.start();
			
			gestionar_sonido(sonido);
			
			
		}


		@Override
		public boolean onKeyUp(int keyCode, KeyEvent event) {
			
			if(keyCode==KeyEvent.KEYCODE_MENU || keyCode==KeyEvent.KEYCODE_SEARCH){
//				
//				Toast.makeText(this, "Tecla Menu", Toast.LENGTH_LONG).show();
//				stopService(new Intent(this, MusicaDeFondo.class));
//				finish();
				
			}
			
			if(keyCode==KeyEvent.KEYCODE_BACK){
				
				finish();
				
			}
			
			return true;
			
		}


	@Override
	public void onClick(View arg0) {
		
		

		if(arg0.getId()==R.id.text_prin_contrareloj){
			
			id_seleccionada=R.id.text_prin_contrareloj;
			
			snd.play(R.raw.transicion_musica,this);
			mp.stop();
			
			iniciar_animacion_final();

		}
		
		if(arg0.getId()==R.id.text_prin_record){
			
			id_seleccionada=R.id.text_prin_record;
			
			snd.play(R.raw.transicion_musica,this);
			mp.stop();
			
			iniciar_animacion_final();

		}
		
		if(arg0.getId()==R.id.text_prin_config){
			
			id_seleccionada=R.id.text_prin_config;
			
			snd.play(R.raw.transicion_musica,this);
			mp.stop();
			
			iniciar_animacion_final();

		}
		
		if(arg0.getId()==R.id.bsonido_main){
			sonido=!sonido;
			
			gestionar_sonido(sonido);

		}
		
		if(arg0.getId()==R.id.bfacebook_main){
			
		       Intent i = new Intent("android.intent.action.VIEW", Uri.parse(Variables.ENLACE_FACEBOOK));
		       startActivity(i);
		}
		
		
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		 
		
		if(animation.equals(anim2)){
			tconfig.setText(R.string.configuracion);
			tcontrareloj.setText(R.string.contrarreloj);
			trecord.setText(R.string.records);
			
			tconfig.startAnimation(anim3);
			tcontrareloj.startAnimation(anim3);
			trecord.startAnimation(anim3);
		}
		if(animation.equals(anim3)){
			imagen.setImageResource(R.drawable.inicio);
			imagen.startAnimation(anim4);
		}
		if(animation.equals(anim4)){
			ttitulo.setText(R.string.app_name);
			ttitulo.startAnimation(anim4v2);
		}
		if(animation.equals(anim5)){
						
			tconfig.startAnimation(anim6);
			tcontrareloj.startAnimation(anim6);
			trecord.startAnimation(anim6);

			lin4.startAnimation(anim6);
			
			imagen.setImageResource(R.drawable.vacio);
			

			

		}
		if(animation.equals(anim6)){
			
			tconfig.setText("");
			tcontrareloj.setText("");
			trecord.setText("");
			
	        lin1.startAnimation(anim7);
	        lin2.startAnimation(anim7);
	        lin3.startAnimation(anim7);
			
			
		
	        bfacebook.setBackgroundResource(R.drawable.vacio);
	        bsound.setBackgroundResource(R.drawable.vacio);
			
	        anim2.setAnimationListener(null);
	        ima_punto1.startAnimation(anim2);
	        ima_punto2.startAnimation(anim2);
	        ima_punto3.startAnimation(anim2);
		}
		if(animation.equals(anim7)){
			
			
			tconfig.setText("");
			tcontrareloj.setText("");
			trecord.setText("");
			
			
			ima_punto1.setImageResource(R.drawable.vacio);
	        ima_punto2.setImageResource(R.drawable.vacio);
	        ima_punto3.setImageResource(R.drawable.vacio);
	        
	        ttitulo.setAnimation(anim8);
	        
		}
		
		if(animation.equals(anim8)){
			
			ttitulo.setText("");
			
			 if(id_seleccionada==R.id.text_prin_config){
		        	
					Intent j = new Intent(this, Configuracion.class);
					startActivity(j);
		        	
	        }
	        if(id_seleccionada==R.id.text_prin_contrareloj){
	        	
					Intent j = new Intent(this, JuegoContrarelojDificultad.class);
					startActivity(j);
	        	
	        }
	        if(id_seleccionada==R.id.text_prin_record){
	        	
					Intent j = new Intent(this, Records.class);
					startActivity(j);
		        	
	        }
		}
		
		
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		
		
	}
	
	private void gestionar_sonido(Boolean sonido) {
		// TODO Auto-generated method stub
			if(sonido==false){
				bsound.setBackgroundResource(R.drawable.sonidooff);
				mp.pause();
				
			}
			
			if(sonido==true){
				bsound.setBackgroundResource(R.drawable.sonidoon);
				mp.start();
			}
	}
	 
	
	 

}
