package GameOfFlags.www;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Configuracion extends Activity implements OnClickListener{
	
	Button bborrar_records,bvivrar,bvalorar,bsonido,bacercade;
	
	boolean vibrar,sonido;
	
	Variables var;
	Datos datos;
	Tools tools;
	
	SharedPreferences prefs_cargar;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config);
        
        var= new Variables();
        
        //tipo de letra de la activity      
        Typeface type_letra = Typeface.createFromAsset(getAssets(), Variables.FUENTE_LETRA_PROGRAMA);
        
        //Asociamos todos los componentes con la letra y escuchamos su llamada
        
        bborrar_records=(Button)findViewById(R.id.bborrar_records);
        bborrar_records.setTypeface(type_letra);
        bborrar_records.setOnClickListener(this);
        
        bvivrar=(Button)findViewById(R.id.bvibrar);
        bvivrar.setTypeface(type_letra);
        bvivrar.setOnClickListener(this);
        
        bvalorar=(Button)findViewById(R.id.bvalorar);
        bvalorar.setTypeface(type_letra);
        bvalorar.setOnClickListener(this);
        
        bsonido=(Button)findViewById(R.id.bsonido_config);
        bsonido.setTypeface(type_letra);
        bsonido.setOnClickListener(this);
        
        bacercade=(Button)findViewById(R.id.bacercade);
        bacercade.setTypeface(type_letra);
        bacercade.setOnClickListener(this);
        
        
    	
    	
        
        cargar_configuracion();
        
        
	}
	
/********************************************************************************************************************************/        
	/*
	 * En esta funcion cargamos toda la configuracion de esta activity
	 */
/********************************************************************************************************************************/        

	
	 private void cargar_configuracion() {
		
		 // leemos la variable booleana vibrar que hemos configurado, y la igualamos a nuestra variable local de esta activity
		 
		 try{
			 prefs_cargar = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
			 vibrar=(Boolean)prefs_cargar.getBoolean(Variables.VIBAR_SWITCH,true);
		 }catch(NullPointerException e){
			 
			 //en caso de que no se encuentre la creamos
			 
			   SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
			   SharedPreferences.Editor editor = prefs.edit();	    
			   editor.putBoolean(Variables.VIBAR_SWITCH,true);
			   editor.commit();
		 }
	     
		 //llamamos al metodo que activar� o seactivar� la vibraci�n
		 
		 Vibrar_Switch(vibrar);
		 
		// leemos la variable booleana sonido que hemos configurado, y la igualamos a nuestra variable local de esta activity
		 
		 
		 try{
			 sonido=(Boolean)prefs_cargar.getBoolean(Variables.SONIDO_SWITCH,true);
		 }catch(NullPointerException e){
			 
			 //en caso de que no se encuentre la creamos
			 
			   SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
			   SharedPreferences.Editor editor = prefs.edit();	    
			   editor.putBoolean(Variables.SONIDO_SWITCH,true);
			   editor.commit();
		 }
	     
		 //llamamos al metodo que activar� o seactivar� el sonido
		 
		 Sonido_Switch(sonido);
	      
	}
	 
		
/********************************************************************************************************************************/        
 	/*
 	 * En esta funcion guardamos toda la configuracion de esta activity
 	 */
/********************************************************************************************************************************/        

	 
		private void guardarConfiguracion() {
			// TODO Auto-generated method stub
			   SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
			   SharedPreferences.Editor editor = prefs.edit();
			   
			   //guardamos el estado de la vibraci�n o true o false
			   editor.putBoolean(Variables.VIBAR_SWITCH,vibrar);
			   editor.putBoolean(Variables.SONIDO_SWITCH,sonido);
			   editor.commit();
		}
		
/********************************************************************************************************************************/        
 	/*
 	 * Ciclo de Vida de la Activity
 	 */
/********************************************************************************************************************************/        


	  @Override
	    public void onDestroy()
	    {
		  //Evento "Cerrar aplicaci�n" guardardamos los datos que queramos en un fichero xml 
	      super.onDestroy();
	      
	    } 
	  
	  	@Override
	    public void onResume()
	    {
		  //Evento "Cerrar aplicaci�n" guardardamos los datos que queramos en un fichero xml 
	      super.onResume();
	      
	      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	      
	    } 

		protected void onStart(){
			 super.onStart();
			 
		 }
	
		@Override
		protected void onStop() {
			super.onStop();

		}
		
		@Override
		protected void onPause() {
			super.onPause();
			
			guardarConfiguracion();
			finish();
		
		}
    
    
/********************************************************************************************************************************/        
 	/*
 	 * Eventos al pulsar los botones
 	 */
/********************************************************************************************************************************/        
	
		public boolean onKeyUp(int keyCode, KeyEvent event) {
			
			if(keyCode==KeyEvent.KEYCODE_HOME || keyCode==KeyEvent.KEYCODE_SEARCH){
				
				//si pulsamos el boton home o el boton buscar

				finish();
				
			}
			
			if(keyCode==KeyEvent.KEYCODE_BACK){
				
				//si pulsamos el boton volver
				Intent j = new Intent(this, Menu_Principal.class);
				startActivity(j);
			}
			return true;
		}
		
/********************************************************************************************************************************/        
 	/*
 	 * Eventos al pulsar los Views
	 */
/********************************************************************************************************************************/        
		
	@Override
	public void onClick(View v) {
		
		//pulsamos el boton para borrar records
		
		if(v.getId()==R.id.bborrar_records){
			
			
			abrir_dialog_alerta();
 
		}
		
		if(v.getId()==R.id.bvibrar){
			
			//pulsamos el boton de Vibrar
			
			vibrar=!vibrar; 
			Vibrar_Switch(vibrar);
			

		}
		
		if(v.getId()==R.id.bvalorar){
			
			//Pulsamos el boton valorar
			
			Abrir_Para_Valorar_App();
 
		}
		
		if(v.getId()==R.id.bsonido_config){
			
			sonido=!sonido; 
			Sonido_Switch(sonido);
			
						
 
		}
		
		if(v.getId()==R.id.bacercade){
			
			
			abrir_dialog_acercade();
 
		}
	}
	
/********************************************************************************************************************************/        
	/*
	 * funcion que consiste en borrar los records, para ello accedemos a nuestro fichero xml en donde tenemos guardado los
	 * records una vez alli le ponemos un valor nulo, en este caso un cero
	 */
/********************************************************************************************************************************/        


	void borrar_records() {

		
		
		SharedPreferences prefs_guardar= getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);		   
    	SharedPreferences.Editor editor = prefs_guardar.edit();
		
		for(int i=0;i<var.s_records.length;i++){
			editor.remove(var.s_records[i]);
			editor.putString(var.s_records[i], "0");
		}
		
		for(int i=0;i<var.s_record_nombre.length;i++){
			editor.remove(var.s_record_nombre[i]);
			editor.putString(var.s_record_nombre[i], "0");
		}
		editor.commit();
		
		
		Toast mensaje= Toast.makeText(this, R.string.mensaje_borrar_records, Toast.LENGTH_SHORT);
		mensaje.show();
		
	}

/********************************************************************************************************************************/        
/*
 * funcion abre una simple ventana del navegador con la direccion de nuestro enlace en el google play
 */
/********************************************************************************************************************************/        

	
	private void Abrir_Para_Valorar_App() {
		// TODO Auto-generated method stub
		
	       Intent i = new Intent("android.intent.action.VIEW", Uri.parse(Variables.ENLACE_PLAY_GOOGLE));
	       startActivity(i);
		
	}
	
/********************************************************************************************************************************/        
/*
 * funcion que cambia la imagen de fondo del boton vibrar
 */
/********************************************************************************************************************************/        


	private void Vibrar_Switch(Boolean v) {
		
	        
		
	        if(v==true){
	        	bvivrar.setBackgroundResource(R.drawable.botonon);
	        	bvivrar.setText(getString(R.string.Vibrar)+getString(R.string.Yes));
	        	
	        }
	        
	        if(v==false){
	        	bvivrar.setBackgroundResource(R.drawable.botononoff);
	        	bvivrar.setText(getString(R.string.Vibrar)+getString(R.string.No));
	        	
	        }
	        
	        
	}
	
private void Sonido_Switch(boolean v) {
    if(v==true){
    	bsonido.setBackgroundResource(R.drawable.botonon);
    	bsonido.setText(getString(R.string.Sonido)+getString(R.string.Yes));
    	
    }
    
    if(v==false){
    	bsonido.setBackgroundResource(R.drawable.botononoff);
    	bsonido.setText(getString(R.string.Sonido)+getString(R.string.No));
    	
    }
}

private void abrir_dialog_acercade() {
	// TODO Auto-generated method stub
	Dialog d= new Dialog(this);
	d.setTitle(getString(R.string.Acercade));
	TextView registro=new TextView(this);
	registro.setText(getString(R.string.contenido_acercade));
	
	ScrollView scroll=new ScrollView(this);
	scroll.addView(registro);
	d.setContentView(scroll);
	d.show();
}

/********************************************************************************************************************************/        
/*
 * funcion que muestra un dialogo de alerta para asegurarse de que no se ha dado por error al boton
 */
/********************************************************************************************************************************/        


	private void abrir_dialog_alerta() {
		
		 final AlertDialog.Builder alertaCompuesta = new AlertDialog.Builder(this);
		 
				 alertaCompuesta.setTitle(getString(R.string.warning));
				 alertaCompuesta.setMessage(getString(R.string.Mensaje_alerta_records));
				 alertaCompuesta.setPositiveButton(getString(R.string.Yes),
						 
				
						 
				 new DialogInterface.OnClickListener() {
				 public void onClick(DialogInterface dialog, int which) {
					 borrar_records();
				 }
				 });
				 alertaCompuesta.setNegativeButton(getString(R.string.No),
				 new DialogInterface.OnClickListener() {
				 public void onClick(DialogInterface dialog, int which) {
				 }
				 });
				 
				 
				 
				 alertaCompuesta.show();
		
	}
}
