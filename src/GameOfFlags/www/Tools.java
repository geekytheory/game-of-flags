package GameOfFlags.www;


import java.util.Random;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Tools extends Activity{


	
	Variables var;

	Vector<Integer> vector_frases;
	Random random;
	
	int x,y,z,w;
	
	Datos datos = new Datos();
	
	

	
	
	/*
	 * esta funcion nos devolvera una sucesion de variables enteras guardadas en un vector que corresponder� 
	 * a los indices de las peliculas. para ello tendr� que leer un String y convertirlo a un entreo.
	 */

		public Vector<Integer> DescifrarIndicePeliculas(String a){
			
		Vector<Integer> v = new Vector<Integer>();
		v.clear();	
		int numerodenumeros=1;
		int numero = 0;
		int numerocompuesto=0;
		String letra;
		
		/*
		 * en este bucle se hace los siguiente: primero se separa cada caracter en un nuevo array llamado letra,
		 * seguidamente se convierte cada caracter en un entero, para terminar se almacena todo en un vector,
		 * el cual devolveremos. 
		 */
		
		for(int i=0;i<a.length();i++){
			
			
			
			letra=a.substring(i,i+1);
			
			//comparamos si el String letra es igual al caracter "-", en caso afirmativo 
			if(letra.equals("-")){
				
				numerodenumeros=numerodenumeros+1;//aumentamos el numero de numeros que tenemos en  nuestro String
				numerocompuesto=0; //inicializamos esta variable que usamos para el caso de que sea
				numero=0;
				
			}else{
				
				try{
					
					numero=Integer.parseInt(letra.toString());//convertimos e igualamos en la variable numero 
					
					if(numerocompuesto==0){//si es un numero menor que diez hacemos esto
						
						numerocompuesto=numero;
						
						try{
							
							v.add(numerocompuesto);
							
						}catch(NullPointerException xxxx){
							
						}
						
					}else{//si es un numero mayor que diez hacemos esto 
							numerocompuesto=numerocompuesto*10+ numero;
							
							try{
								
								v.remove(numerodenumeros-1);
								v.add(numerocompuesto);
								
							}catch(NullPointerException xxxx){
								
							}
						 }
				}catch(NumberFormatException e){
					
				}	
			}	
		}

		return v;
		
		}
	
	/*********************************************************************************************************/
	
		public int Numero_Aleatorio(int lim_max){
			
			Random x;
		    x=new Random();
		    int numerosalido =x.nextInt(lim_max);
			
			return numerosalido;
		}
		
		
		public Boolean Numero_Aleatorio_No_Usado_con_Anterioridad(int numeroaleatorio,Vector<?> v) {
		
			int x=numeroaleatorio;
			Boolean devolver=true;
			
			if(v.isEmpty()==true){
				devolver = true;
			}else{
				for(int i=0; i<v.size();i++){
					if(x==(Integer)v.get(i)){
						devolver = false;
						break;
					}else{
						devolver = true;
					}
				}
			}
			return devolver;
		
		}

		/*********************************************************************************************************/
		
		/*
		 * Funcion que nos genera en un vector un numero aleatorio de frases de peliculas, para ello introducimos el valor del numero de frases 
		 * que queremos.
		 */
		public Vector<Integer> Frases_Aleatorias (int numerofrasesaleatorias,int posicion_indice_codigo,String nivel){
			
			var = new Variables();		
			vector_frases= new Vector<Integer>();
			random=new Random();	
			
			int posicion_aleatoria=0;
			
			for(int i=0;i<numerofrasesaleatorias;i++){
				int x2=0;
				int pelicula=0;
				
				if(nivel.equals(Variables.FACIL)){
					
					x2 = random.nextInt(datos.titulos_peliculas_id_facil.length);
					
					pelicula = datos.titulos_peliculas_id_facil[x2];

				}
				
				if(nivel.equals(Variables.MEDIO)){
					
					x2 = random.nextInt(datos.titulos_peliculas_id_medio.length);
					
					pelicula = datos.titulos_peliculas_id_medio[x2];

				}
				
				if(nivel.equals(Variables.DIFICIL)){
					
					x2 = random.nextInt(datos.titulos_peliculas_id_dificil.length);
					
					pelicula = datos.titulos_peliculas_id_dificil[x2];

				}

				vector_frases.add(pelicula);
				
							
				
			}
			
			//a�adimos la solucion en una casilla aleatoria 
			
			if(nivel.equals(Variables.FACIL)){
				
				posicion_aleatoria = random.nextInt(Variables.NUMERO_BOTONES);
				
				vector_frases.set(posicion_aleatoria, (int) datos.titulos_peliculas_id_facil[posicion_indice_codigo]);
				
			}
			
			if(nivel.equals(Variables.MEDIO)){
				
				posicion_aleatoria = random.nextInt(Variables.NUMERO_BOTONES);
				
				vector_frases.set(posicion_aleatoria, (int) datos.titulos_peliculas_id_medio[posicion_indice_codigo]);
			}
			
			if(nivel.equals(Variables.DIFICIL)){
				
				posicion_aleatoria = random.nextInt(Variables.NUMERO_BOTONES);
				
				vector_frases.set(posicion_aleatoria, (int) datos.titulos_peliculas_id_dificil[posicion_indice_codigo]);
			}
			
			
			
			return vector_frases; 
		}
		
/************************************************************************************************************************/

		//comprobamos si existe algun String duplicado dentro del vector_frases, en caso afirmativo devuelve false
		
		
		    Boolean frasesduplicadas(Vector<?> vector_frases) {
			
			
			int a=0;
			
			Boolean duplicado=true;
			
			for(int i=0;i<vector_frases.size();i++){
				
				a=(Integer) vector_frases.get(i);
				
				for(int k=i+1;k<vector_frases.size();k++){
					if(a==(Integer)vector_frases.get(k)){
						
					
						duplicado=false;
				}
				
			}
			}
			return duplicado;
		}
		    
		    /************************************************************************************************************************/

		  // dos enteros y nos devuelve true o false en funcion de si son o no iguales
		    
		public boolean Comprobar_Si_Dos_Numeros_Son_Iguales(int indice_pelicula,int indice_codigo) {
			
			
			boolean devolver=false;
			
			
			if(indice_pelicula==indice_codigo){
				devolver=true;
			}
			
			
			return devolver;
}
		
		
	   /************************************************************************************************************************/
	
	@SuppressLint("WorldWriteableFiles")
	public void guardar_preferencia(String nombreficheroxml,String Key,String texto_que_quiero_guardar){
		
		
		SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_WORLD_WRITEABLE);
		
		SharedPreferences.Editor editor = prefs.edit();
		
		
		editor.putString(Key, texto_que_quiero_guardar);
		editor.commit();
		
		
		
	}
	
	public String cargar_preferencia(String nombreficheroxml,String Key){  
	      
	      SharedPreferences prefs = getSharedPreferences(nombreficheroxml,Context.MODE_PRIVATE);
	      String valorLeido = prefs.getString(Key, "ERROR");
	      return valorLeido;
		
		
		
	}
	
	
			
			
		
		

		
		
				
}


 