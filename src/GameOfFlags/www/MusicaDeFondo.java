package GameOfFlags.www;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class MusicaDeFondo extends Service{
	
	public MediaPlayer mp;
	public int a=0;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	
		mp = MediaPlayer.create(this, R.raw.theme_cj);
		mp.setLooping(true);
		
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();
		mp.pause();
		a=mp.getCurrentPosition();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		mp.seekTo(a);
		mp.start();
		
	}
	
	

}
