package GameOfFlags.www;

import java.util.Vector;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class Variables{
	
	//datos de configuracion
	
	public static final String NOMBRE_TEXTEDIT_CJ_FINALIZAR = "nombre_listview_contrareloj_finalizar";
	public static String NOMBRE_FICHERO_PREFERENCIAS_RECORDS="ficheroconfiguracion";
	public static String RECORD_GENERAL="Record_General";
	public static String SCORE_CONSEGUIDO_EN_LA_PARTIDA="Score";
	public static String NOMBRE_RECORD_GENERAL="Record_Nombre_General";
	public static String VIBAR_SWITCH="VIBRAR";
	public static String SONIDO_SWITCH="SONIDO";
	public static String NOMBRE_RECORD_GENERAL_POR_DEFECTO="Smiley";
	public static String FUENTE_LETRA_PROGRAMA = "SHOWG.TTF";
	public static String BOOLEAN_VALORAR_APP = "valorarapp";
	
	public static String PREFERENCIA_ENTERO_MUSICA = "preference_entero_musica";

	public static int TOTAL_PELICULAS=10;
	public static int NUMERO_BOTONES=4;
	
	public static int NUMERO_IMAGEVIEW_CJ=10;
	
	public static int ACIERTO_CJ=500;
	public static int FALLO_CJ=-300;
	
	
	public static int SEGUNDOS_TIEMPO_CONTRARELOJ_CJ=30; 
	public static int MINUTOS_CONTRARELOJ_CJ=1; 
	public static long TIEMPO_CONTRARELOJ_CJ=500000; 
	public static long ESCALADO_TIEMPO_CONTRARELOJ_CJ=1000; //en milis 
	public static int SEGUNDOS_A�ADIR_CON_COMODIN=15000; //en milis 15seg
	public static int SCORE_ANADIR_COMODIN=1000; 
	
	public static final String ENLACE_PLAY_GOOGLE = "https://play.google.com/store/apps/details?id=GameOfFlags.www";
	public static final String ENLACE_FACEBOOK = "https://www.facebook.com/GameOfFlags";

	
	public static final String NIVEL_DIFICULTAD = "nivel_dificultad";
	public static final String FACIL = "facil";
	public static final String MEDIO = "medio";
	public static final String DIFICIL = "dificil";
	
	
	/**************************************************************************************************************************/
	
	/*
	 * Los imageviews que corresponden con la ventana de nuestro juego.
	 */
	public ImageView ima_score00,ima_score01,ima_score02,ima_score03;

	ImageView imagenes_score []= {ima_score00,ima_score01,ima_score02,ima_score03};
	
	public int [] id_imagenes_score = {
			R.id.score00,
			R.id.score01,
			R.id.score02,
			R.id.score03
			
	};
	
	private ImageView image00;
	
	
	
	public int [] id_imagenes_cj = {
			R.id.ImageView00,
		
	};
	
	ImageView imagenes_cj []= {image00};
	
	
	ImageView comodin00,comodin01,comodin02,comodin03,comodin04;
	
	public int [] id_comodines_cj = {
			R.id.comodin00,
			R.id.comodin01,
			R.id.comodin02,
			R.id.comodin03,
			R.id.comodin04,
		
	};
	
	ImageView comodines_cj []= {comodin00,comodin01,comodin02,comodin03,comodin04};
	
	/**************************************************************************************************************************/
	
	
	/*
	 * Los botones que corresponden con la ventana de nuestro juego.
	 */
	
	private Button boton00,boton01,boton02,boton03;
	

	Button botones_cj []={boton00,boton01,boton02,boton03};
	
	public int [] id_botones_cj = {R.id.button00,R.id.button001,R.id.Button02,R.id.Button03};
	
	/**************************************************************************************************************************/
	
	/*
	 * Los botones que corresponden con la ventana de nuestro juego.
	 */
	
	LinearLayout lin_imagen_cj,lincomodin_cj;
	
	/**************************************************************************************************************************/
	
	/*
	 * Vectores.
	 */
	
	public Vector<?> vector_iconos_cj,vector_peliculas_cj;
	
	/**************************************************************************************************************************/
	
	/*
	 * Enteros
	 */
	
	public int indice_codigo_cj=0;
	public int indice_pelicula_cj=0;
	
	int acierto_cj=0;
	int fallos_cj=0;
	int num_score_cj=0;
	int num_comodines_cj=0;
	
	
	/**************************************************************************************************************************/
	
	/*
	 * TextView 
	 */
	TextView tscore_cj,ttime_cj,texto_cj;
	
	//records
	
	TextView trecord0,trecord1,trecord2,trecord3,trecord4;
	TextView trecord_nombre1,trecord_nombre2,trecord_nombre3,trecord_nombre4,trecord_nombre5;
	
	TextView [] tRecords ={trecord0,trecord1,trecord2,trecord3,trecord4};
	
	int [] id_trecords = {
			R.id.trecord0,
			R.id.trecord1,
			R.id.trecord2,
			R.id.trecord3,
			R.id.trecord4,
	};
	
TextView [] tRecord_nombre ={trecord_nombre1,trecord_nombre2,trecord_nombre3,trecord_nombre4,trecord_nombre5};
	
	int [] id_trecord_nombre = {
			R.id.trecord_nombre1,
			R.id.trecord_nombre2,
			R.id.trecord_nombre3,
			R.id.trecord_nombre4,
			R.id.trecord_nombre5,
	};
	
	String [] s_records ={
		"Record1",
		"Record2",
		"Record3",
		"Record4",
		"Record5",
	};
	
	String [] s_record_nombre ={
			"Record_nombre1",
			"Record_nombre2",
			"Record_nombre3",
			"Record_nombre4",
			"Record_nombre5",
		};
	
	/**************************************************************************************************************************/
	


	
		
	
	
	
	
}
