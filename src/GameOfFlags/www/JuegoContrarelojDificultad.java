package GameOfFlags.www;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;

public class JuegoContrarelojDificultad extends Activity implements  OnClickListener,OnTouchListener{
	
  	 Button bfacil,bmedio,bdificil;
  	 String nivel="";
	
  	 
  	 
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contrareloj_dificultad);
        

        Typeface type_letra = Typeface.createFromAsset(getAssets(), Variables.FUENTE_LETRA_PROGRAMA);
 	
        bfacil=(Button)findViewById(R.id.bfacil);
        bmedio=(Button)findViewById(R.id.bmedio);
        bdificil=(Button)findViewById(R.id.bdificil);
        
        bfacil.setOnClickListener((android.view.View.OnClickListener) this);
        bmedio.setOnClickListener((android.view.View.OnClickListener) this);
        bdificil.setOnClickListener((android.view.View.OnClickListener) this);
        
        bfacil.setOnTouchListener(this);
        bmedio.setOnTouchListener(this);
        bdificil.setOnTouchListener(this);
        

        bfacil.setTypeface(type_letra);
        bmedio.setTypeface(type_letra);
        bdificil.setTypeface(type_letra);
        
	}
	
	@Override	
	protected void onPause() {
		super.onPause();
		finish();
	}
	
	@Override	
	protected void onResume() {
		super.onResume();
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	}


	@Override
	public void onClick(View v) {

		
		if(v.getId()==bfacil.getId()){
			
			nivel = Variables.FACIL;
			
		}
		if(v.getId()==bmedio.getId()){
			
			nivel = Variables.MEDIO;
			
		}
		if(v.getId()==bdificil.getId()){
			
			nivel = Variables.DIFICIL;
		}
		
		guardar();
		
		Intent i = new Intent(this, JuegoContrareloj.class);
		startActivity(i);
		
	}
	


	private void guardar() {

		   SharedPreferences prefs = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);   
		   SharedPreferences.Editor editor = prefs.edit();
		   
		   editor.putString(Variables.NIVEL_DIFICULTAD, nivel);
		   editor.commit();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		if(event.getAction()==MotionEvent.ACTION_DOWN){
			
			if(v.getId()==bfacil.getId()){
				bfacil.setBackgroundResource(R.drawable.botonon);
			}
			
			if(v.getId()==bmedio.getId()){
				bmedio.setBackgroundResource(R.drawable.botonon);
			}
			
			if(v.getId()==bdificil.getId()){
				bdificil.setBackgroundResource(R.drawable.botonon);
			}
			
		}
		
		if(event.getAction()==MotionEvent.ACTION_UP){
			if(v.getId()==bfacil.getId()){
				bfacil.setBackgroundResource(R.drawable.botonesdefault);
			}
			
			if(v.getId()==bmedio.getId()){
				bmedio.setBackgroundResource(R.drawable.botonesdefault);
			}
			
			if(v.getId()==bdificil.getId()){
				bdificil.setBackgroundResource(R.drawable.botonesdefault);
			}
		}
		
		
		return false;
	} 
	
	@Override
	public void onBackPressed() {
		
		Intent j = new Intent(this, Menu_Principal.class);
		startActivity(j);

		return;
	}



}
