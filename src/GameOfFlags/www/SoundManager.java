package GameOfFlags.www;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundManager {
	
	private Context pContext;
	private SoundPool sndPool;
	private float rate =100.0f;
	private float masterVolume =1.0f;
	private float leftVolume =100.0f;
	private float rightVolume =100.0f;
	private float balance =0.5f;
	
	public SoundManager(Context appContext){
		sndPool= new SoundPool(16, AudioManager.STREAM_MUSIC, 100);
		pContext = appContext;
	}
	
	public int load(int sound_id){
		return sndPool.load(pContext, sound_id, 1);
	}
	 
	public void play(int sound_id,Context c){
	    AudioManager mgr = (AudioManager)c.getSystemService(Context.AUDIO_SERVICE);
	    float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
	    float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
	    float volume = streamVolumeCurrent / streamVolumeMax;
		sndPool.play(sound_id, volume, volume, 1, 0, 1f);
	}
	
	public void unloadAll(){
		sndPool.release();
	}
	
}
