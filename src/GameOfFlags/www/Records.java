package GameOfFlags.www;

import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Records extends Activity implements AnimationListener, OnLoadCompleteListener {
	
	Variables var;

	
	String record_general="";
	String nombre_record_general="";
	
	Boolean animation_barras=true;
	
	SharedPreferences prefs_cargar,prefs_guardar;
	SharedPreferences.Editor editor;
	Vector<String> vector_records;
	Vector<String> vector_records_nombres;
	
	ImageView ima_barra1,ima_barra2;
	
	TextView thigh_score;
	
	LinearLayout lin_records;
	
	Animation anim1,anim2,anim3,anim4,anim5,anim6,anim7,anim8;
	
	Boolean sonido=true;

	SoundPool sp;
	int bien = 0;

	
//	MediaPlayer mp;
	
	

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.records);
        
        var = new Variables();

        
        Typeface type_letra = Typeface.createFromAsset(getAssets(), Variables.FUENTE_LETRA_PROGRAMA);
        
        for(int i=0; i< var.tRecords.length;i++){
        	var.tRecords[i]=(TextView)findViewById(var.id_trecords[i]);
        	var.tRecords[i].setTypeface(type_letra);
        	var.tRecord_nombre[i]=(TextView)findViewById(var.id_trecord_nombre[i]);
        	var.tRecord_nombre[i].setTypeface(type_letra);
        }
        
        
        	
        	ima_barra1=(ImageView)findViewById(R.id.ima_barra_1);
        	ima_barra2=(ImageView)findViewById(R.id.ima_barra_2);
        	
        	lin_records=(LinearLayout)findViewById(R.id.lin_high_score);
        	
        	thigh_score=(TextView)findViewById(R.id.thigh_score);
        	thigh_score.setTypeface(type_letra);
		
        	prefs_cargar = getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);
        	
        	prefs_guardar= getSharedPreferences(Variables.NOMBRE_FICHERO_PREFERENCIAS_RECORDS,Context.MODE_PRIVATE);		   
        	editor = prefs_guardar.edit();
        	 		
        	
        //	snd = new SoundManager(getApplicationContext());
        //	this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        	
        //	snd.play(snd.load(R.raw.metalhit), this);
        	
//        	sp = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);
//        	
//        	AssetManager assetManager = null;
//        	AssetFileDescriptor descriptor = null;
//			try {
//				descriptor = assetManager.openFd("prueba.mp3");
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//        	
//        	bien= sp.load(descriptor,1);
//        	
//        	sp.play(bien, 1, 1, 0, 0, 1);
        	
    		sp = new SoundPool(8, AudioManager.STREAM_MUSIC, 0);
    		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        	bien= sp.load(this,R.raw.metalhit,1);
        	
        	sp.setOnLoadCompleteListener(this);
        	
//			mp = MediaPlayer.create(this, R.raw.theme_cj);
//			mp.setLooping(true);
        	
        	
    		borrar_componentes();
    		
        	

        
	}

    private void dibujar_pantalla() {
		
		
        for(int i=0; i< var.tRecords.length;i++){
        	var.tRecords[i].setBackgroundResource(R.drawable.botonrecordv1);
        }
        
        for(int i=0; i< var.tRecord_nombre.length;i++){
        	var.tRecord_nombre[i].setBackgroundResource(R.drawable.botonrecordv1);
        }
		thigh_score.setText(R.string.high_score);
		
	}

	private void borrar_pantalla() {
		
		
        for(int i=0; i< var.tRecords.length;i++){
        	var.tRecords[i].setBackgroundResource(R.drawable.vacio);
        }
        
        for(int i=0; i< var.tRecord_nombre.length;i++){
        	var.tRecord_nombre[i].setBackgroundResource(R.drawable.vacio);
        }
		thigh_score.setText("");
	}

	public void onDestroy()
    {
      super.onDestroy();
      
      
      
      if(animation_barras==false){
    	  guardarRecords();
      }
      
	 
     
    } 
	
	public void onResume()
    {
      super.onResume();
 
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

      
    }
    


	//evento "Abrir aplicación" leemos configuración del fichero xml
    @Override
    protected void onStart()
    {
      super.onStart();
            
    }    



	   
	@Override
	protected void onPause() {
		
		super.onPause();
	

		
		
	      if(animation_barras==false){
	    	  guardarRecords();
	      }
	      
//	      mp.pause();
	      
	      guardarRecords();
	      
	      finish();
	}
    

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		
		if(keyCode==KeyEvent.KEYCODE_HOME || keyCode==KeyEvent.KEYCODE_SEARCH){
			
			
		}
		
		if(keyCode==KeyEvent.KEYCODE_BACK){
			// do something on back.
			
			   if(animation_barras==false){
			    	  iniciar_animacion_final();
			      }
				
			
		}
		
		return true;
		
	}
	
	private void cargarRecords() {
		
		
//		flujo_musica=prefs_cargar.getInt(Variables.PREFERENCIA_ENTERO_MUSICA,0);
//		
//		if(flujo_musica!=0){
//			
//			mp.seekTo(flujo_musica);
//			mp.start();
//		}else{
//			mp.start();
//		}
	     
	    /*
	     * aqui cargamos los records en los textView   
	     */
		
	      record_general = prefs_cargar.getString("Record_General", "0");
	      nombre_record_general=prefs_cargar.getString("Record_Nombre_General", "0"); 
	      OrdenarRecords();
	}
	



	private void guardarRecords() {
		
		
		for(int i=0;i<var.s_records.length;i++){
			editor.putString(var.s_records[i], vector_records.get(i)+"");
			editor.putString(var.s_record_nombre[i], vector_records_nombres.get(i)+"");
		}
//		
//			editor.putInt(Variables.PREFERENCIA_ENTERO_MUSICA,mp.getCurrentPosition());
		   		   
			editor.commit();
		   
	}
	
	private void OrdenarRecords() {
		// TODO Auto-generated method stub
		vector_records =new Vector<String>();
		vector_records_nombres= new Vector<String>();
		
		int x=0,y=0;
		String a="";
		String b="";
		
		for(int i=0;i<var.s_records.length;i++){
			vector_records.add(prefs_cargar.getString(var.s_records[i], "0"));
			vector_records_nombres.add(prefs_cargar.getString(var.s_record_nombre[i], "0"));
		}
		
		try{
			x=Integer.parseInt(record_general);
			
			for(int i=0;i<vector_records.size();i++){
				y=Integer.parseInt(vector_records.get(i)+"");
				
				if(x>=y){
					a=vector_records.get(i)+"";
					b=vector_records_nombres.get(i)+"";
					
					vector_records.setElementAt(record_general,i);
					vector_records_nombres.setElementAt(nombre_record_general, i);
					x=y;
					
					record_general=a;
					nombre_record_general=b;
				}
				
			}
					
		}catch(NumberFormatException e){
			
		}
		
		editor.remove("Record_General");
		editor.putString("Record_General", "0");
		editor.remove("Record_Nombre_General");
		editor.putString("Record_Nombre_General", "0");
		
	      for(int i=0; i<var.tRecords.length;i++){
	    	  
	    	  if((vector_records.get(i)+"").equals("0")){
	    		  
	    		  var.tRecords[i].setText("");
	     	  }else{
	     		 var.tRecords[i].setText(vector_records.get(i)+""); 
	     	  }
	  
	      }
	      
	      for(int i=0; i<var.tRecord_nombre.length;i++){
	    	  
	    	  if((vector_records_nombres.get(i)+"").equals("0")){
	    		  
	    		  var.tRecord_nombre[i].setText("");
	     	  }else{
	     		 var.tRecord_nombre[i].setText(vector_records_nombres.get(i)+""); 
	     	  }
	  
	      }
	      
	      for(int i=0;i<var.tRecord_nombre.length;i++){
	    	  if(var.tRecord_nombre[i].getText().equals("") || var.tRecords[i].getText().equals("")){
	    		  var.tRecord_nombre[i].setText("");
	    		  var.tRecords[i].setText("");
	    	  }
	    	  
	      }
	}

	private void iniciar_animacion_principal() {
		
		
		anim1 = AnimationUtils.loadAnimation(this, R.anim.translate_barra_records1);
		anim1.reset();
		anim1.setAnimationListener(this);
		anim2 = AnimationUtils.loadAnimation(this, R.anim.translate_barra_records2);
		anim2.reset();
		anim2.setAnimationListener(this);
		anim3 = AnimationUtils.loadAnimation(this, R.anim.alpha);
		anim3.reset();
		anim3.setAnimationListener(this);
		anim4 = AnimationUtils.loadAnimation(this, R.anim.translate4);
		anim4.reset();
		anim4.setAnimationListener(this);
		

		
	borrar_pantalla();
		
		ima_barra1.setAnimation(anim1);
		ima_barra2.setAnimation(anim2);
		animation_barras=true;
		
	}

	private void iniciar_animacion_final() {

		
		
		anim5 = AnimationUtils.loadAnimation(this, R.anim.alpha_descendiente);
		anim5.reset();
		anim5.setAnimationListener(this);
		anim6 = AnimationUtils.loadAnimation(this, R.anim.translate_centro_arriba);
		anim6.reset();
		anim6.setAnimationListener(this);
		anim7 = AnimationUtils.loadAnimation(this, R.anim.translate_centro_izq);
		anim7.reset();
		anim7.setAnimationListener(this);
		anim8 = AnimationUtils.loadAnimation(this, R.anim.translate_centro_der);
		anim8.reset();
		anim8.setAnimationListener(this);
		
		lin_records.startAnimation(anim5);
		thigh_score.startAnimation(anim6);
		
		animation_barras=true;
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		
		if(animation.equals(anim1)){
			dibujar_pantalla();
			lin_records.startAnimation(anim3);
			thigh_score.startAnimation(anim4);
			animation_barras=true;
		}
		
		if(animation.equals(anim4)){
			animation_barras=false;
			cargarRecords();
		}
		
		if(animation.equals(anim6)){
			
			if(bien!=0 && sonido==true){
				sp.play(bien, 1, 1, 0, 0, 1);
			}
			
			thigh_score.setText("");
			animation_barras=true;
			
			for(int i=0;i<var.tRecord_nombre.length;i++){
				var.tRecord_nombre[i].setText("");
				var.tRecords[i].setText("");
				var.tRecord_nombre[i].setBackgroundResource(R.drawable.vacio);
				var.tRecords[i].setBackgroundResource(R.drawable.vacio);
				
			}
			
			ima_barra1.startAnimation(anim7);
			ima_barra2.startAnimation(anim8);
			
		}
		if(animation.equals(anim8)){
			ima_barra1.setImageResource(R.drawable.vacio);
			ima_barra2.setImageResource(R.drawable.vacio);
			
			animation_barras=false;
			
			
			Intent j = new Intent(this, Menu_Principal.class);
			startActivity(j);
			
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		
		
	}
	

	@Override
	public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
		// TODO Auto-generated method stub
		
		sonido=prefs_cargar.getBoolean(Variables.SONIDO_SWITCH, true);
		
		if(bien!=0 && sonido == true){
			
				sp.play(bien, 1, 1, 0, 0, 1);
		}
		
		
		restaurar_componentes();
		iniciar_animacion_principal();
	}

	private void borrar_componentes() {
		// TODO Auto-generated method stub
		
		
		ima_barra1.setImageResource(R.drawable.vacio);
		ima_barra2.setImageResource(R.drawable.vacio);
		
		thigh_score.setText("");
		
		lin_records.setVisibility(View.INVISIBLE);
	}

	private void restaurar_componentes() {
		// TODO Auto-generated method stub
		ima_barra1.setImageResource(R.drawable.barra_1);
		ima_barra2.setImageResource(R.drawable.barra_2);
		
		thigh_score.setText(getText(R.string.high_score));
		
		lin_records.setVisibility(View.VISIBLE);
	}


}
